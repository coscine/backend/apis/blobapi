﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.Blob
{
    /// <summary>
    /// Standard Program Class
    /// </summary>
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        /// <summary>
        /// Standard main body
        /// </summary>
        public static void Main()
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = int.MaxValue;

            InitializeWebService<Startup>();
        }
    }
}