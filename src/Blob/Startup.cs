﻿using Coscine.ApiCommons;
using Coscine.ResourceTypes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;

namespace Coscine.Api.Blob
{
    /// <summary>
    /// Standard Startup class.
    /// </summary>
    public class Startup : AbstractStartup
    {
        /// <summary>
        /// Standard Startup constructor.
        /// </summary>
        public Startup()
        {
        }

        /// <summary>
        /// Configures custom service extensions
        /// </summary>
        /// <param name="services">services</param>
        public override void ConfigureServicesExtension(IServiceCollection services)
        {
            base.ConfigureServicesExtension(services);

            services.AddHttpClient();
        }

        /// <summary>
        /// Add HttpClinetFactory to resource types
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public override void ConfigureExtensionLate(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.ConfigureExtensionLate(app, env);

            using var scope = app.ApplicationServices.CreateScope();
            ResourceTypeFactory.HttpClientFactory = scope.ServiceProvider.GetRequiredService<IHttpClientFactory>();
        }
    }
}